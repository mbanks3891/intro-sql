/* Use  the Chinook  database loaded into your installation of
MySQL to complete this assignment. 

Coding summary queries
Insert your queries below the instruction comments.
When you are finished with all of the select statements, save the file as a .sql script using this
naming convention
lastnamelab5.sql.
Submit the .sql file for grading. 
************************************************************************
TEST YOUR QUERIES BEFORE SUBMITTING!!!!
***********************************************************************
If you don't understand the expected results required of a certain question 
please contact your instructor before you submit the assignment */

/*  1. 
Write a SELECT statement that returns 
the count of the number of artists in the artist table.
*/

SELECT COUNT(*) FROM artist;


 
/*  2. 
Write a SELECT statement that displays the highest invoice, 
lowest invoice, and average invoice of all of the inoices in the 
invoice table. Use the Total (column(?)) to determine each of these. Use column 
aliases of your choosing for each of these, and round the average summary to two decimal places. */

SELECT  COUNT(total), SUM(total), MAX(total) as "highest invoice", MIN(total) 
as "lowest invoice",  ROUND(AVG(total),2) as "average invoice"  from invoice;



/*  3. 
Count each TrackId that exists in the invoiceline table, but only count each TrackId one time */ 

SELECT COUNT(DISTINCT trackid) FROM invoiceline;



/*  4. 
Provide the total dollar amount of all of the invoices in the invoice 
table for all invoices with a BillingCountry  of 'Brazil', 'Chile', or 'Argentina' */

SELECT SUM(total) FROM invoice WHERE billingcountry = 'Brazil' or 'Chile' or 'Argentina';



/* 5. 
Count the number of invoices by each BillingCountry. 
To count each instance of an invoice in the data you must use a 
GROUP BY function - display the country name and the counts. */

SELECT billingcountry, COUNT(*) as "count" from invoice GROUP BY billingcountry ORDER BY billingcountry ASC;



/*  6. 
Count the number of tracks for each genre. Display the GenreId 
and the Name of the genre as well as the counts. */

SELECT g.genreid, g.name as "genre name", COUNT(*) as "count/qty"
FROM track t
LEFT JOIN genre g
on  t.genreid = g.genreid
GROUP BY t.genreid ORDER BY "count/qty" DESC;

-- check: 
SELECT * from track WHERE genreid = 5;



/*  7. 
Write a SELECT statement that returns the average length of each track 
by album as shown in the Miliseconds column of the track table, 
return the AlbumId, the album Title, the ArtistId, and the Name of the artist */

SELECT al.artistid, ar.name as "artist name", al.albumid, al.title as "album title", AVG(tr.milliseconds) as "avg track length"
FROM ALBUM al
LEFT JOIN ARTIST ar  -- only join artists that have an album
ON ar.artistid = al.artistid
LEFT JOIN TRACK tr  -- only join tracks that have an artist (and an album)
ON tr.albumid = al.albumid
GROUP BY al.title 
ORDER BY ar.name ASC;



/* 8. 
For each state in the United States that exists in the customer table, 
determine the number of CustomerId, List the State and the number of customers. 
Don't list customers in countries other than the USA.  */

SELECT state, COUNT(customerid) 
FROM customer 
WHERE state IS NOT NULL and country="USA"
GROUP BY state
ORDER BY state ASC;




/* 9. 
Provide the total dollar amount of invoices for each customer in 
the Invoice table that has more than one invoice, return the CustomerId and this totalled amount */ 

SELECT c.customerid, SUM(i.total) as "invoice total" 
FROM customer c
LEFT JOIN invoice i   
on c.customerid = i.customerid
GROUP BY c.customerid
HAVING COUNT(i.total) > 1;

-- check:
SELECT SUM(total) 
FROM invoice
WHERE customerid = "1";



/* 10. 
Write a SELECT statement that totals all the invoice amounts by SupportRepId. 
Return the supportrepid and the total. Sort the results so that the support rep id 
with the largest total of invoices is at the top. */

SELECT  c.supportrepid,  SUM(i.total) as "invoice sum"
FROM customer c
LEFT JOIN invoice i
on i.customerid = c.customerid
GROUP BY c.supportrepid
ORDER BY "invoice sum" ;



/* 11. 
For each album in the database, display the AlbumId, the album's Name, 
the shortest track, the longest track and the average track length 
(as shown in the miliseconds column of the track table). */
SELECT al.albumid, al.title as "album name", 
MIN(tr.milliseconds) as "Album's Shortest Track Length", 
MAX(tr.milliseconds) as "Album's Longest Track Length", 
AVG(tr.milliseconds) as "Album's Average Track Length"
FROM album al
LEFT JOIN track tr
ON tr.albumid = al.albumid
GROUP BY al.title
ORDER BY al.albumid ASC;


/* 12. 
Write a SELECT statement that returns one row for each employee in the employee table. 
Additionally return the count of customers for each employee. 
If the employee has no associated customers list the employee anyway. 
Display the  employee first name and last name and the  count of customers. */

SELECT em.employeeid, em.firstname, em.lastname, COUNT(cu.customerid) as "number of customers"
FROM employee em
LEFT JOIN customer cu -- return data from 'customer' for all employees 
ON cu.supportrepid = em.employeeid
GROUP BY em.employeeid
;



/* 13. 
Use the ROLLUP function and a group by function to total all invoiceline Quantities 
by invoice in the invoiceline table as well as a grand total of all the quantities. 
Display the InvoiceId and the total Quantity for each invoice as well as the grand total. */
SELECT invoiceid, SUM(quantity) as "invoice qty"
FROM invoiceline
GROUP BY invoiceid WITH ROLLUP;

-- check:
SELECT COUNT(invoicelineid)
FROM invoiceline;



/*  14. 
Use two grouping levels and the ROLLUP function to count 
the number of customers by  country, and within each country by state*/

SELECT c1.state, c2.country, 
COUNT(c1.customerid)  as "states's customer qty", 
COUNT(c2.customerid)  as "country's customer qty"
FROM customer c1
INNER JOIN customer c2
ON c1.customerid = c2.customerid
GROUP BY c2.country, c1.state WITH ROLLUP 
;



/* 15. 
List the top 5 artists in terms of total sales. 
List the artist Name, and the sum of all invoices by the artist, 
biggest (in terms of total sales, to smallest). 
Hint : to do this you will need to join artist to album to track to 
invoiceline to invoice, sum the Total of each invoice, 
group by the artist name or artistid and sort the list in descending order with a limit clause. */
SELECT ar.name, SUM(iv.total)
FROM invoice iv
JOIN invoiceline il
ON iv.invoiceid = il.invoiceid
JOIN track tr
ON il.trackid = tr.trackid
JOIN album al
ON al.albumid = tr.albumid
JOIN artist ar
ON al.artistid = ar.artistid
GROUP BY ar.name
ORDER BY SUM(iv.total) DESC
LIMIT 5
;







