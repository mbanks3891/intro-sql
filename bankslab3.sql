/* Use the chinook database to complete this lab. */
/* Unless otherwise noted use explicit syntax for this lab */
/* Insert a comment line with your last name, which lab this is and the date*/ 
/* When you are finished save your work as a .sql script using the convention lastnamelab3.sql */ 
/* Submit this script for grading */

-- Banks, Lab 3, 6-21-2019




/*1. 
Write a select statement that joins the album table to the artist table 
and returns these columns for every Album in the database: 
AlbumId, Title, ArtistId, and the artist's Name 
*/
-- CORRECT
SELECT album.albumid, album.title, artist.artistid, artist.name
FROM album  -- apply selections
INNER JOIN artist -- apply selections
ON album.artistid = artist.artistid; -- require matching 'artistid' on both sides

-- INCORRECT
SELECT *
FROM album
INNER JOIN artist -- duplicates 'artistid' because we're selecting all from both sides
ON album.artistid = artist.artistid;


-- INCORRECT
SELECT albumid, title -- not pulling 'artistid' from album
FROM album
INNER JOIN artist -- fails to return info from artist because the ON condition has nothing to match to
ON album.artistid = artist.artistid;




/* 2. 
For each Invoice in the database list the InvoiceId, the InvoiceDate, the CustomerId, and Total. 
Also include the customer's LastName, and Country. Add a clause so that the result is sorted by country A-Z, 
and within each country by invoice total - largest to smallest.  
*/
SELECT i.invoiceid, i.invoicedate, i.customerid, i.total, c.lastname, c.country
FROM invoice i -- assign alias
INNER JOIN customer c -- assign alias
ON i.customerid = c.customerid -- require matching 'customerid' on both sides
ORDER BY c.country ASC, i.total DESC;




/* 3. 
Write a select statement that joins the invoice table to the invoice_line table and returns these columns: 
InvoiceId, InvoiceDate, CustomerId, Quantity, and UnitPrice. 
The query should  only return orders for customers 16, 34, 37. 
*/
SELECT i.invoiceid AS "InvoiceId", i.invoicedate AS "i.InvoiceDate", i.customerid AS "i.CustomerId", 
l.quantity AS "L.Quantity", l.unitprice AS "L.UnitPrice" -- clarifying which table each field came from
FROM invoice i
INNER JOIN invoiceline l
ON i.invoiceid = l.invoiceid
WHERE i.customerid IN ('16','34','37');




/* 4. 
Return the same result as question 3, but this time use implicit syntax 
*/
SELECT i.invoiceid AS "InvoiceId", i.invoicedate AS "i.InvoiceDate", i.customerid AS "i.CustomerId", 
l.quantity AS "L.Quantity", l.unitprice AS "L.UnitPrice" -- clarifying which table each field came from
FROM invoice i, invoiceline l
WHERE i.invoiceid = l.invoiceid  -- require matching 'invoiceid' on both sides
AND i.customerid IN ('16','34','37');




/* 5. 
Return the same result as question 3, but this time use the USING keyword 
*/
SELECT i.invoiceid AS "InvoiceId", i.invoicedate AS "i.InvoiceDate", i.customerid AS "i.CustomerId", 
l.quantity AS "L.Quantity", l.unitprice AS "L.UnitPrice" -- clarifying which table each field came from
FROM invoice i
JOIN invoiceline l USING (invoiceid) -- require matching 'invoiceid' on both sides
WHERE i.customerid IN ('16','34','37');




/* 6. 
Return the same result as question 3, but this time use table aliases, 
you may use implicit syntax, the USING keyword, or any syntax you choose that works,  
but use table aliases 
*/ 
SELECT i.invoiceid AS "InvoiceId", i.invoicedate AS "i.InvoiceDate", i.customerid AS "i.CustomerId", 
l.quantity AS "L.Quantity", l.unitprice AS "L.UnitPrice" -- clarifying which table each field came from
FROM invoice i -- table alias is "i"
INNER JOIN invoiceline l -- table alias is "l"
ON i.invoiceid = l.invoiceid
WHERE i.customerid IN ('16','34','37');




/* 7. 
Write a select statement that joins the album and the track tables. 
Display these columns: AlbumId, Title, TrackId, Name, Composer, and Miliseconds. 
Only return rows where the composer contains the text 'Clapton' 
*/
SELECT a.albumid AS "AlbumId", a.title AS "a.Title", t.trackid AS "t.TrackId", 
t.name AS "t.name", t.composer AS "t.composer", t.milliseconds AS "t.Milliseconds"
FROM album a
JOIN track t
ON a.albumid = t.albumid-- require matching 'albumid' on both sides
WHERE t.composer LIKE "%Clapton%"; -- CORRECTED: contains "Clapton" between start/end of string





/* 8. 
For each row in the invoiceline table return the following information: 
InvoiceId, TrackId, InvoiceDate, , CustomerId, customers' LastName, and Phone 
*/
 SELECT l.invoiceid AS "L.InvoiceId", l.trackid AS "L.TrackId", i.invoicedate AS "i.InvoiceDate", 
 i.customerid AS "i.CustomerId", c.lastname AS "c.LastName", c.phone AS "c.Phone"
 FROM invoiceline l
 JOIN invoice i
 ON l.invoiceid = i.invoiceid -- require matching 'invoiceid' on both sides
 JOIN customer c 
 ON i.customerid = c.customerid; -- require matching 'customerid' on both sides
 
 
 
/* 9.	
For every employee in the database write a query that shows the EmployeeId, the LastName, and the HireDate. 
If the employee has customers assigned to them display the CustomerId and the customer's email address. 
Sort the list so that employees without customers are listed at the top. 
Note: the tables are joined on a customer column called SupportRepId 
*/
SELECT e.employeeid AS "e.EmployeeId", e.lastname AS "e.LastName", e.hiredate AS "e.HireDate", 
c.lastname AS "c.LastName", c.email AS "c.E-Mail"
FROM employee e
LEFT JOIN customer c  
-- include all rows from table on the left(employees) 
-- but only matching rows from table on the right(customer) 
-- aka return null for the 'customer' rows if blank, but still show data from the corresponding 'employee' rows
ON e.employeeid = c.supportrepid -- require matching 'emp/rep id' on both sides
ORDER BY c.lastname IS NULL DESC, e.lastname ASC ;




/* 10. 
Write a query that displays the AlbumId, the album Title, the TrackId, the Composer of the track,  
the MediaTypeId, and the media Name for any track whose composer contains 'Jimmy Page' and whose AlbumId is 44 
 */
 SELECT a.albumid, a.title, t.trackid, t.composer, t.mediatypeid, t.name
 FROM album as a
 INNER JOIN  track as t
 ON a.albumid = t.albumid
 WHERE  a.albumid="44" and t.composer LIKE "%Jimmy Page%";  -- % wildcard operator on both sides to find if t.composer contains string


 
 
 
 

/* 11.  
Join the customer table to itself so that only customers located in the same city and country as other customers are displayed. 
Display the CustomerId, the City, and Country for each customer. 
*/ 

SELECT c1.customerid, c1.city, c1.country -- c2.customerid, c2.city, c2.country 
FROM customer c1
INNER JOIN customer c2
ON c1.customerid <> c2.customerid  -- customer id should not match, AKA seeking unique ids side to side
AND  c1.city = c2.city -- city on each side must match
AND c1.country = c2.country; -- country on each side must match





/* 12.	
Use the UNION operator to generate a result set consisting of four columns from the invoice table:  
InvoiceId, 
InvoiceDate,
Total,
And a calculated column called Destination: if the Invoice has a Country of USA display the words 'Domestic' otherwise display the words 'International'
 */
SELECT invoiceid, invoicedate, total, CONCAT(billingcountry,"- domestic") AS Destination
FROM invoice
WHERE billingcountry="usa"
UNION
SELECT invoiceid, invoicedate, total, CONCAT(billingcountry, "- international") 
FROM invoice
WHERE billingcountry<>"usa";





/* 13. 
Produce a query that displays the employee FirstName, LastName, Customer FirstName, LastName, 
InvoiceId, InvoiceDate, Total, TrackId, track Name for every row in the invoiceline table. 
Use column aliases to distinguish the customer first and last name and the employee first and last name. 
*/
SELECT CONCAT(e.firstname, ' ', e.lastname) AS 'employee name', 
CONCAT(c.firstname, ' ', c.lastname) AS 'customer name', 
i.invoiceid, l.invoicelineid, i.invoicedate, (i.total) AS "invoice total", l.trackid, t.name
FROM invoiceline l
LEFT JOIN invoice i
ON l.invoiceid = i.invoiceid
LEFT JOIN track t
ON l.trackid = t.trackid
JOIN customer c 
ON c.customerid = i.customerid
JOIN employee e
ON c.supportrepid= e.employeeid
ORDER BY l.invoicelineid ASC;


/* 14. 
Write a query that displays the CustomerId, the InvoiceDate, 
and the Total for any invoice that includes a track whose genre is 'Jazz' 
*/
SELECT  i.invoicedate, i.total, c.customerid, l.trackid -- g.genreid, i.invoiceid, t.genreid
FROM invoice i  -- get invoicedate and invoicetotal from 'invoices'
JOIN customer c -- get customerid from 'customers'
ON c.customerid = i.customerid -- but only show customers that have an invoice associated
JOIN invoiceline l -- get trackid from 'invoiceline'
ON l.invoiceid = i.invoiceid -- but only show invoice lines from invoices that have customer associated
JOIN track t
ON t.trackid = l.trackid -- only show tracks from those invoicelines
JOIN genre g
ON t.genreid = g.genreid -- show genreid for all tracks that occur on our invoicelines
WHERE g.genreid = 2; -- but filter for genreid 2 aka jazz
;



/* 15. 
For every artist whose Name starts with 'A' or 'B' display the artist Name, 
any AlbumId and album Name associated with the artists, any TrackId and track Names 
associated with the artists, and the associated GenreId and genre Names associated with the track. 
Use column aliases to distinguish the four Name columns
(note: there are some artists who have no albumns) 
*/

select a.name as 'artist name', al.albumid, al.title as 'album title', 
tr.trackid, tr.name as 'track name',  tr.genreid, (g.name) as 'genre name', tr.albumid
FROM artist a 
LEFT JOIN album al  -- CORRECTED: LEFT JOIN of album to artist will show all 'artists'(left) and any matching 'album' rows(right)
ON a.artistid = al.artistid
INNER JOIN track tr
ON al.albumid = tr.albumid
INNER JOIN genre g
ON tr.genreid = g.genreid
WHERE a.name LIKE 'A%' OR a.name LIKE 'B%'
ORDER BY a.name ASC, al.title ASC;


