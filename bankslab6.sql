
-- Michael Banks, Lab 6, 7-7-2019



/* Use the my guitar shop database to generate 12 queries. 
Insert a comment line at the top of the editor indicating your name, the lab this is, 
and the date of submission. Save your queries as a script file named lastnamelab5.sql. 
Submit the sql script file for grading using the link in the lab.  */


/*1.	
Write a SELECT statement that returns the same result set as this SELECT statement, but don’t use a join. 
Instead, use a subquery in a WHERE clause that uses the IN keyword. 
This query would list category names only if there are products associated with the category.  
SELECT DISTINCT category_name 
FROM categories c 
JOIN products p 
ON c.category_id = p.category_id 
ORDER BY category_name */


SELECT DISTINCT category_name
FROM categories  
WHERE category_id IN 
	(SELECT category_id
	FROM products)
;



/*2. 
Use a subquery to display the order_id, product_id, and quantity for each row in the order_items table. 
Only display rows with orders where the customer used a Visa credit card. 
*/ 
SELECT order_id as "order_id from Order Items", product_id as "product_id from Order Items", 
quantity as "quantity from Order Items"
FROM order_items
WHERE order_id IN  -- where order_id from 'order_items' table...
	(SELECT order_id 
	FROM orders  -- matches order_id from 'orders' table...
	WHERE card_type = 'visa') -- but only where card_type from 'orders' table is 'visa'
;





/*3. 
Repeat your results from question 2, but this time include the product name as well. 
*/
SELECT oi.order_id as "order_id from Order Items", oi.product_id as "product_id from Order Items", 
oi.quantity as "quantity from Order Items", product_name as"product name from Products"
FROM order_items oi
LEFT JOIN products pr
ON oi.product_id = pr.product_id
WHERE order_id IN  -- where order_id from 'order_items' table...
	(SELECT order_id 
	FROM orders  -- matches order_id from 'orders' table...
	WHERE card_type = 'visa') -- but only where card_type from 'orders' table is 'visa'
;





/*4.	
For each order where the customer is not using a Visa credit card list 
the order_id, product_id, and quantity stored in the order_items table – use a subquery. 
*/
SELECT order_id as "order_id from Order Items", product_id as "product_id from Order Items", 
quantity as "quantity from Order Items"
FROM order_items
WHERE order_id IN  -- where order_id from 'order_items' table...
	(SELECT order_id 
	FROM orders  -- matches order_id from 'orders' table...
	WHERE card_type != 'visa') -- but only where card_type from 'orders' table is 'visa'
;




/*5.	
Write a SELECT statement with a subquery that determines the customer_id and the last_name  
of the customer whose credit card expires soonest as shown in the orders table. 
*** to do this join two tables and use a subquery in a WHERE clause ***
*/
SELECT cu.customer_id, cu.last_name
FROM customers cu
JOIN orders od
ON  cu.customer_id = od.customer_id 
JOIN order_items oi
ON od.order_id = oi.order_id
WHERE od.order_id =
	(SELECT MIN(card_expires)
	FROM orders)
;



/*6.	
Use the IN keyword with a subquery to list the customer_id, first_name, and last_name 
of all customers whose  shipping address is different than their billing address. 
Hint: use a grouping procedure in the subquery and only count customers who have more 
than 1 address in the addresses table. 
OR write a subquery that generates the list by 
comparing shipping_address _id to the billing_address_id in the customers table.  
*/

-- first way: grouping by customers with more than one id
SELECT c.customer_id, c.first_name, c.last_name, c.shipping_address_id, c.billing_address_id
FROM customers c 
WHERE customer_id IN
	(SELECT customer_id
    FROM addresses a
    GROUP BY a.customer_id
    HAVING COUNT(a.customer_id) > 1)
;

-- second way: subquery compares shipping address and billing address
SELECT c.customer_id, c.first_name, c.last_name, c.shipping_address_id, c.billing_address_id
FROM customers c 
WHERE customer_id IN
	(SELECT customer_id
    FROM customers c
    WHERE shipping_address_id != billing_address_id)
;









/*7.	
Use a subquery with a NOT EXISTS operator to list the customer_id,  
last_name, and email address of every customer that does not have an order. 
*/

SELECT c.customer_id, c.last_name, c.email_address
FROM customers c
WHERE NOT EXISTS
	(SELECT o.customer_id
    FROM orders o
    WHERE o.customer_id = c.customer_id)  -- without this WHERE, will only get null.  Need to match customer_id in order/customer tables.
    -- aka WHERE this matched id between order/customer tables does NOT exist
;




/*8.	
Use a subquery to calculate the average tax amount 
from each order if the tax amount is not zero, 
then list the order_ids, customer_id, and tax_amount  
of the orders with tax amounts higher than the average. 
*/

SELECT  order_id, customer_id, tax_amount
FROM orders
WHERE tax_amount >
	(SELECT AVG(tax_amount)
	FROM orders
	WHERE tax_amount != 0)
;

 












 
/*9.	
Write a SELECT statement that includes: 
all customer_ids and last_names from the customers table, as well as the most recent order_date for each customer 
that has orders from the orders table. 
Only include one order_date per customer,  this order_date should be the most recent order as shown in the order_date column. 
Name this column "Newest". 
Use a subquery in the SELECT clause of the SELECT statement  to accomplish this. 
One customer does not have orders list that customer_id and last_name as well - so remember this will be a LEFT JOIN of the two tables. 
Also remember that there is a an operator called DISTINCT that you can use to list each customer only one time
*/

SELECT c.customer_id, c.first_name, c.last_name, MAX(o.order_date) as "Newest"
FROM customers c
LEFT JOIN orders o
ON c.customer_id = o.customer_id
GROUP BY  c.customer_id    
;
-- USED 'GROUP BY' BECAUSE I COULD NOT GET DESIRED RESULTS BY USING SUBQUERY :(








 

  

/*10 
Use a subquery and the ALL operator to list the order_id and ship_amounts 
of any order that is greater than the ship amount of all of the orders placed by customer_id 4 
*/
SELECT o.order_id, o.ship_amount
FROM orders o
WHERE o.ship_amount > ALL
	(SELECT SUM(ship_amount)
    FROM orders
    WHERE customer_id = 4)
    ;


/*11.	
Use a subquery and outer query to determine the last_name, 
first_name, and email address of all customers with credit_cards 
that expire in 2013 or 2014 as shown in the orders table.  
*/
SELECT c.last_name, c.first_name, c.email_address, o.card_expires
FROM customers c
LEFT JOIN orders o
ON o.customer_id = c.customer_id
WHERE card_expires IN
	(SELECT card_expires
    FROM customers
    WHERE card_expires LIKE "%2014%" OR card_expires LIKE "%2016%")
;


/*12.	
USE an ALL operator with a subquery to display the category_name,  
category_id, product_id,   product_name and list_price of every product 
that has a list price greater than  any product that has a category id = 2.   
*/

SELECT product_name, category_id, product_id, product_name, list_price
FROM products
WHERE list_price > ALL
	(SELECT list_price
    FROM products
    WHERE category_id = 2)
;